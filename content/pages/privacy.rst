:slug: privacy
:save_as: privacy.html

Privacy Policy
~~~~~~~~~~~~~~

The GNU Mailman software collects only your ``real name``, email address
and subscription preferences. Your ``real name`` is optional and need not be
provided. Your email address is used in sending you messages from the email
lists to which you subscribe and for verifying your identity when you
subscribe to lists, visit list archives or make certain other changes to your
subscription settings. Your ``real name``, email address and subscription
preferences are available to the administrators of the lists to which you
subscribe. In addition, Your ``real name`` and email address are included in
rosters available to the list administrators, and, depending on list settings,
may also be available to other list members.

Also, many lists maintain public archives which are web accessible and
indexed in search engines. Other lists maintain archives which are available
only to list subscribers, but may allow anyone to subscribe. Anything you post
to a list, including your name and email address and any information in the
post or its email signature is potentially available to anyone.
